import { Routes, Route, useLocation, Navigate } from 'react-router-dom';
import AgregarPractica from './fragment/AgregarPracticaEstudiante';
import AgregarPracticaProfesor from './fragment/AgregarPracticaProfesor';

import InicioEstudiante from './fragment/InicioEstudiante';
import Login from './fragment/Login';
import ListaCursos from './fragment/ListaCursos';
import TareasCreadas from './fragment/TareasCreadas';
import CrearTareas from './fragment/CrearTareas';
import Actualizar from './fragment/Actualizar';
import PresentarEstudiante from './fragment/PresentarEstudiante';
import ListarProfesor from './fragment/ListarProfesor';
import TareasEnviadasEstudiante from './fragment/CrearTareas';
import ListaTareasPendientes from './fragment/ListaTareasPendientes';
import { estaSesion } from './utilidades/Sessionutil';
import InicioProfesor from './fragment/InicioProfesor';
import InicioAdmin from './fragment/InicioAdmin';
import ListarMaterias from './fragment/ListarMaterias';


function App() {

  const Middeware = ({children}) => {
    const autenticado = estaSesion();
    const location = useLocation();
    if (autenticado) {
      return children;
    } else {
      return <Navigate to= '/sesion' state={location}/>;
    }
  }
  const MiddewareSesion = ({children}) => {
    const autenticado = estaSesion();
    const location = useLocation();
    if (autenticado) {
      return <Navigate to= '/inicio' />;
    } else {
      return children;
    }
  }
  return (
    <div className="App">
      <Routes>
        <Route path='/' element={<Login/>}/>
        <Route path='/InicioEstudiante' element={<InicioEstudiante />} />
        <Route path='/InicioAdmin' element={<InicioAdmin />} />
        <Route path='/ListaCursos' element={<ListaCursos/>} />
        <Route path='/TareasCreadas/:external' element={<TareasCreadas/>} />
        <Route path='/CrearTareas/:external' element={<CrearTareas/>} />
        <Route path='/estudiantess' element={<Actualizar/>} />
        <Route path='/estudiantes' element={<PresentarEstudiante/>}/>
        <Route path='/VerTarea/:external' element={<TareasEnviadasEstudiante/>} />
        <Route path='/AgregarPractica' element={<AgregarPractica/>} />
        <Route path='/PresentarEstudiante' element={<PresentarEstudiante/>} />
        <Route path='/ListarProfesor' element={<ListarProfesor/>} />
        <Route path='/ListaTareasPendientes' element={<ListaTareasPendientes />} />
        <Route path='/ListarMaterias' element={<ListarMaterias />} />
        <Route path='/InicioProfesor' element={<InicioProfesor />} />
        <Route path='/TareasCreadas/:external' element={<AgregarPracticaProfesor />} />
      </Routes>
    </div>
  );
}
export default App;