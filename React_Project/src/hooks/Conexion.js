const URL = "http://localhost:3011/api"
export const InicioSesion = async (data) => {
  const headers = {
    'Accept': 'application/json',
    "Content-Type": "application/json",

  };
  const datos = await (await fetch(URL + "/sesion", {
    method: "POST",
    headers: headers,
    body: JSON.stringify(data)
  })).json();
  return datos;
}
export const obtenerMateria = async () => {
  try {
    const cabeceras = { "X-API-KEY": "token" }; // Reemplaza "tu_token" con el valor de tu token
    const respuesta = await fetch(URL + "/cursa/materias", {
      method: "GET",
      headers: cabeceras
    });

    if (!respuesta.ok) {
      throw new Error("Error al obtener los datos de materias");
    }

    const datos = await respuesta.json();
    return datos.info; // Solo devolvemos la propiedad "info" de la respuesta

  } catch (error) {
    console.error(error);
    // Manejar el error de alguna manera adecuada en tu aplicación
  }
};
export const obtenerExternal = async () => {
  try {
    const cabeceras = { "X-API-KEY": "token" }; // Reemplaza "tu_token" con el valor de tu token
    const respuesta = await fetch(URL + "/cursa/external", {
      method: "GET",
      headers: cabeceras
    });

    if (!respuesta.ok) {
      throw new Error("Error al obtener los datos de autos");
    }

    const datos = await respuesta.json();
    return datos.info; // Solo devolvemos la propiedad "info" de la respuesta

  } catch (error) {
    console.error(error);
    // Manejar el error de alguna manera adecuada en tu aplicación
  }
};
export const obtenerPractica = async () => {
  try {
    const cabeceras = { "X-API-KEY": "token" }; // Reemplaza "tu_token" con el valor de tu token
    const respuesta = await fetch(URL + "/practicas/listar", {
      method: "GET",
      headers: cabeceras
    });

    if (!respuesta.ok) {
      throw new Error("Error al obtener los datos de autos");
    }

    const datos = await respuesta.json();
    return datos.info; // Solo devolvemos la propiedad "info" de la respuesta

  } catch (error) {
    console.error(error);
    // Manejar el error de alguna manera adecuada en tu aplicación
  }
};
export const ObtenerTarea = async (key, num) => {
  const headers = {
      "X-API-TOKEN": key,
      "Accept": 'aplication/json',
      "Content-Type": 'application/json'
  };
  const datos = await (await fetch(URL + "/tarea/obtener/" + num, {
      method: "GET",
      headers: headers
  })).json();
  console.log("DATO OBTENIDO =", JSON.stringify(datos));
  return datos;
}
export const obtenerTodo = async () => {
  try {
    const cabeceras = { "X-API-KEY": "token" }; // Reemplaza "tu_token" con el valor de tu token
    const respuesta = await fetch(URL + "/cursa/listar", {
      method: "GET",
      headers: cabeceras
    });

    if (!respuesta.ok) {
      throw new Error("Error al obtener los datos de autos");
    }

    const datos = await respuesta.json();
    return datos.info; // Solo devolvemos la propiedad "info" de la respuesta

  } catch (error) {
    console.error(error);
    // Manejar el error de alguna manera adecuada en tu aplicación
  }
};
export const obtenerListaTareas = async () => {
  try {
    const cabeceras = { "X-API-KEY": "token" }; // Reemplaza "tu_token" con el valor de tu token
    const respuesta = await fetch(URL + "/cursa/tareas", {
      method: "GET",
      headers: cabeceras
    });

    if (!respuesta.ok) {
      throw new Error("Error al obtener los datos de autos");
    }

    const datos = await respuesta.json();
    return datos.info; // Solo devolvemos la propiedad "info" de la respuesta

  } catch (error) {
    console.error(error);
    // Manejar el error de alguna manera adecuada en tu aplicación
  }
};

export const obtenerProfesor = async () => {
  try {
    const cabeceras = { "X-API-KEY": "token" }; // Reemplaza "tu_token" con el valor de tu token
    const respuesta = await fetch(URL + "/profesor", {
      method: "GET",
      headers: cabeceras
    });

    if (!respuesta.ok) {
      throw new Error("Error al obtener los datos de autos");
    }

    const datos = await respuesta.json();
    return datos.info; // Solo devolvemos la propiedad "info" de la respuesta

  } catch (error) {
    console.error(error);
    // Manejar el error de alguna manera adecuada en tu aplicación
  }
};


export const obtenerListaEntregadas = async () => {
  try {
    const cabeceras = { "X-API-KEY": "token" }; // Reemplaza "tu_token" con el valor de tu token
    const respuesta = await fetch(URL + "/tarea/estado", {
      method: "GET",
      headers: cabeceras
    });

    if (!respuesta.ok) {
      throw new Error("Error al obtener los datos de autos");
    }

    const datos = await respuesta.json();
    return datos.info; // Solo devolvemos la propiedad "info" de la respuesta

  } catch (error) {
    console.error(error);
    // Manejar el error de alguna manera adecuada en tu aplicación
  }
};

//----------------
export const GuardarPractica = async (data) => {
  const headers = {
    'Accept': 'application/json',
    "Content-Type": "application/json",

  };
  const datos = await (await fetch(URL + "/tarea/guardar", {
    method: "POST",
    headers: headers,
    body: JSON.stringify(data)
  })).json();
  return datos;
};

export const GuardarMateria = async (data) => {
  const headers = {
    'Accept': 'application/json',
    "Content-Type": "application/json",

  };
  const datos = await (await fetch(URL + "/materia/guardar", {
    method: "POST",
    headers: headers,
    body: JSON.stringify(data)
  })).json();
  return datos;
};
export const ModificarPractica = async (data) => {
  const headers = {
    'Accept': 'application/json',
    "Content-Type": "application/json",

  };
  const datos = await (await fetch(URL + "/tarea/modificar", {
    method: "POST",
    headers: headers,
    body: JSON.stringify(data)
  })).json();
  return datos;
};
//estudiante gusrdar

export const GuardarEstudiante = async (data) => {
  const headers = {
    'Accept': 'application/json',
    "Content-Type": "application/json",

  };
  const datos = await (await fetch(URL + "/estudiantes/guardar", {
    method: "POST",
    headers: headers,
    body: JSON.stringify(data)
  })).json();
  return datos;
};

export const Roles = async (key) => {
  const cabeceras = {
      "X-API-TOKEN": key,
      "Accept": 'aplication/json',
      "Content-Type": 'application/json'
  };
  const datos = await (await fetch(URL + "/roles", {
      method: "GET",
      headers: cabeceras
  })).json();
  console.log(datos);
  return datos;
}

export const Estudiantes = async (key) => {
  const cabeceras = {
      "X-API-TOKEN": key,
      "Accept": 'aplication/json',
      "Content-Type": 'application/json'
  };
  const datos = await (await fetch(URL + '/estudiantes', {
      method: "GET",
      headers: cabeceras
  })).json();
  return datos;
}
export const Profesores = async (key) => {
  const cabeceras = {
      "X-API-TOKEN": key,
      "Accept": 'aplication/json',
      "Content-Type": 'application/json'
  };
  const datos = await (await fetch(URL + '/profesor', {
      method: "GET",
      headers: cabeceras
  })).json();
  return datos;
}

export const ModificarEstudiante = async (key, data) => {
  console.log("Estudiante ha modifcar ", data);
  const headers = {
      "Content-Type": "application/json",
      "X-API-TOKEN": key,
      "Accept": 'aplication/json'
  };
  console.log(URL + "/estudiantes/modificar");
  const datos = await (await fetch(URL + "/estudiantes/modificar", {
      method: "POST",
      headers: headers,
      body: JSON.stringify(data)
  })).json();
  console.log("Estudiante modifcado", datos);
  return datos;
}

export const ObtenerEstudiante = async (key, num) => {
  const headers = {
      "X-API-TOKEN": key,
      "Accept": 'aplication/json',
      "Content-Type": 'application/json'
  };
  const datos = await (await fetch(URL + "/estudiantes/obtener" + num, {
      method: "GET",
      headers: headers
  })).json();
  console.log("DATO OBTENIDO =", JSON.stringify(datos));
  return datos;
}