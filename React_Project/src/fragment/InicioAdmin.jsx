import React from 'react';
import '../css/Indice.css';
import { useNavigate } from 'react-router';

const InicioAdmin = () => {
  const navigation = useNavigate();
  const handleMaterias = () => {
    // Realiza la lógica de inicio de sesión aquí
    // ...

    // Redirige al usuario a la página de inicio
    navigation('/ListarMaterias');
  };

  const handleEstudiantes = () => {
    // Realiza la lógica de inicio de sesión aquí
    // ...

    // Redirige al usuario a la página de inicio
    navigation('/PresentarEstudiante');
  };

  const handleProfesores = () => {
    // Realiza la lógica de inicio de sesión aquí
    // ...

    // Redirige al usuario a la página de inicio
    navigation('/ListarProfesor');
  };
 
  const handlesalir = () => {
    // Realiza la lógica de inicio de sesión aquí
    // ...

    // Redirige al usuario a la página de inicio
    navigation('/');
  };
  const TareasCreadas = () => {
    // Realiza la lógica de inicio de sesión aquí
    // ...
    // Redirige al usuario a la página de inicio
    navigation('/login');
  };
  return (

    <div className='InicioWindow'>
      <div className='NavSuperior' id='NavSuperior'>
        <nav className="navbar navbar-dark bg-dark navbar-expand-lg navbar-light bg-light">
          <ul className="navbar-nav ml-auto">
           
            <li className="nav-item dropdown no-arrow mr-9">
              <a className="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onClick={TareasCreadas}>Perfil</a>
            </li>
            <li className="nav-item dropdown no-arrow mr-9">
              <a className="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Configuraciones</a>
            </li>
            <li className="nav-item dropdown no-arrow mr-9">
              <a className="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onClick={handlesalir}>Salir</a>
            </li>
            
          </ul>
        </nav>
      </div>


      <div className='NavLateral' id='NavLateral'>
        <ul className="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

          <a className="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
            <div className="sidebar-brand-icon rotate-n-15">
              <i className="fas fa-laugh-wink"></i>
            </div>
            <div className="sidebar-brand-text mx-3">ADMINISTRADOR</div>
          </a>
          <hr className="sidebar-divider my-0" />
          <hr className="sidebar-divider my-0" />

          <li className="nav-item active">
            <a className="collapse-item" href="#" onClick={handleMaterias}>MATERIAS</a>
          </li>

          <li className="nav-item active">
            <a className="collapse-item" href="#" onClick={handleEstudiantes}>ESTUDIANTES</a>
          </li>
          <li className="nav-item active">
            <a className="collapse-item" href="#" onClick={handleProfesores}>PROFESORES</a>
          </li>
          
          <li className="nav-item">
            <a className="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages" />
          </li>
        </ul>
      </div>
      
    </div>


  )
};

export default InicioAdmin;
