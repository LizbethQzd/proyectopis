import React, { useState, useEffect } from 'react';
import '../css/visualizar.css';
import { useForm } from 'react-hook-form';
import { GuardarPractica } from '../hooks/Conexion';
import { getToken } from '../utilidades/Sessionutil';
import mensajes from '../utilidades/Mensaje';
import { useNavigate, useParams } from 'react-router-dom';
const CrearTareas = () => {
  const { external } = useParams();
  const navegation = useNavigate();
  const { register, handleSubmit, formState: { errors } } = useForm();
  const onSubmit = (data) => {
    console.log(`Agregar tarea para la materia con ID ${external}`);
    var datos = {
      "external_id": external,
      "fecha_entrega": data.fecha,
      "tema": data.tema,
      "descripcion": data.descripcion
    };
    GuardarPractica(datos, getToken()).then((info) => {
      console.log(info);
      if (info.code !== 200) {
        mensajes(info.msg, 'error', 'Exitoso');
      } else {
        mensajes(info.msg);
      }
    });
  };

  return (
    <form className="vh-100" onSubmit={handleSubmit(onSubmit)}>
      <div className="container h-100">
        <div className="row d-flex justify-content-center align-items-center h-100">
          <div className="col-xl-9">
            <h1 className="text-black mb-2">Agregar Practica</h1>
            <div className="card">
              <div className="card-body">
                <div className="row align-items-center py-3">
                  <div className="col-md-3 ps-5">
                    <h6 className="mb-0">FECHA DE ENTREGA </h6>
                  </div>
                  <div className="col-md-9 pe-5">
                    <input type="date" {...register('fecha', { required: true })} className="form-control form-control-user" placeholder="Ingrese la fecha de emision" />
                    {errors.fecha && errors.fecha.type === 'required' && <div className='alert alert-danger'>Ingrese la fecha de emision</div>}
                    <div className="small text-muted mt-2">Esta será la fecha de entrega</div>
                  </div>
                </div>
                <hr className="mx-n3" />
                <div className="row align-items-center pt-4 pb-3">
                  <div className="col-md-3 ps-5">
                    <h6 className="mb-0">TEMA: </h6>
                  </div>
                  <div className="col-md-9 pe-5">
                    <input
                      type="text"
                      id="form3Example3"
                      className="form-control form-control-lg"
                      placeholder='Ingrese el tema de la practica'
                      {...register('tema', { required: true })}
                    />
                  </div>
                </div>
                <hr className="mx-n3" />
                <div className="row align-items-center py-3">
                  <div className="col-md-3 ps-5">
                    <h6 className="mb-0">DESCRIPCION </h6>
                  </div>
                  <div className="col-md-9 pe-5">
                    <input
                      type="text"
                      id="form3Example3"
                      className="form-control form-control-lg"
                      placeholder="Ingrese la descripcion"
                      {...register('descripcion', { required: true })}
                    />
                    {errors.descripcion && errors.descripcion.type === 'required' && <div className='alert alert-danger'>Ingrese una descripción</div>}
                  </div>
                </div>
                <button className="btn btn-primary" type='submit'>Agregar</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>

  );
}

export default CrearTareas;