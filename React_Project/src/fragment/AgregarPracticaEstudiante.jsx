import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate, useParams } from 'react-router-dom';
import mensajes from '../utilidades/Mensaje';
import { getToken } from '../utilidades/Sessionutil';
import { GuardarPractica } from '../hooks/Conexion';
const AgregarPracticaEstudiante = () => {
    const { register, handleSubmit, formState: { errors } } = useForm();
    const { external } = useParams();
    const onSubmit = (data) => {
        console.log(`Agregar tarea para la materia con ID ${external}`);
        var datos = {
            "external_id": external,
            "fecha_entrega": data.fecha_entrega,
            "tema": data.tema,
            "descripcion": data.descripcion
        };
        GuardarPractica(datos, getToken()).then((info) => {
            console.log(info);
            if (info.code !== 200) {
                mensajes(info.msg, 'error', 'Exitoso');
            } else {
                mensajes(info.msg);
            }
        });

    };

    return (

        <section className="vh-100" onSubmit={handleSubmit(onSubmit)}>

            <div className="container h-100">
                <div class="row d-flex justify-content-center align-items-center h-100">
                    <div class="col-xl-9">

                        <center><h1>Subir Tarea</h1></center>
                        <div className="row d-flex justify-content-center align-items-center h-100">
                            <div className="col-xl-9">
                                <div className="card" >
                                    <div className="card-body">

                                        <div className="row align-items-center py-3">
                                            <hr className="mx-n3" />
                                            <div className="col-md-3 ps-5">

                                                <h6 className="mb-2">ADJUNTO: </h6>

                                            </div>
                                            <div className="col-md-9 pe-5">


                                                <input
                                                    type="file"
                                                    id="form3Example3"
                                                    className="form-control form-control-lg"
                                                    placeholder='Ingrese la practica'
                                                    {...register('tema', { required: true })}
                                                />
                                                <div className="small text-muted mt-2">Sube tu archivo/. Tamaño máximo de archivo 50 MB</div>

                                            </div>
                                        </div>
                                        <hr className="mx-n3" />
                                        <div className="row align-items-center pt-4 pb-3">
                                            <div className="col-md-3 ps-5">

                                                <h6 className="mb-0">GUARDAR COMO: </h6>

                                            </div>
                                            <div className="col-md-9 pe-5">


                                                <input
                                                    type="text"
                                                    id="form3Example3"
                                                    className="form-control form-control-lg"
                                                    placeholder="Ingrese la descripcion"
                                                    {...register('descripcion', { required: true })}
                                                />
                                                {errors.descripcion && errors.descripcion.type === 'required' && <div className='alert alert-danger'>Ingrese una descripción</div>}

                                            </div>
                                        </div>

                                        <hr className="mx-n3" />

                                        <div className="row align-items-center py-3">
                                            <div className="col-md-3 ps-5">

                                                <h6 className="mb-0">FECHA ENTREGA: </h6>

                                            </div>
                                            <div className="col-md-9 pe-5">


                                                <input type="date" {...register('fecha', { required: true })} className="form-control form-control-user" placeholder="Ingrese la fecha de emision" />
                                                {errors.fecha_entrega && errors.fecha_entrega.type === 'required' && <div className='alert alert-danger'>Ingrese la fecha de emision</div>}
                                                <div className="small text-muted mt-2">Esta será la fecha de entrega</div>

                                            </div>
                                        </div>

                                        <hr className="mx-n3" />


                                        <div className="px-12 py-12">
                                        <button className="btn btn-primary" type='submit'>SUBIR</button>

                                        </div>
                                        <hr className="mx-n3" />
                                        <hr className="mx-n3" />


                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>

    );
}
export default AgregarPracticaEstudiante;