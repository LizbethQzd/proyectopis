import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Modal, Input } from 'react-bootstrap';
import RegistrarProfesor from "./RegistrarProfesor";
import EditarEstudiante from "./EditarEstudiante";
import DataTable from "react-data-table-component";
import React, { useState } from 'react';
import { Profesores } from "../hooks/Conexion";
import { borrarSession, getToken } from "../utilidades/Sessionutil";
import mensajes from "../utilidades/Mensaje";
import { useNavigate } from "react-router";
import Footer from "./Footer";


export const ListarProfesor = () => {
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const [data, setData] = useState([]);
    const navegation = useNavigate();
    const [llProfesores, setLlProfesores] = useState(false);//para listar estudiantes
    const actualizar = () => setLlProfesores(false);//para listar estudiantes
    const [llVendidos, setLlVendidos] = useState(false);//para listar vendidos
    const vendidos = () => setLlVendidos(true);//para listar vendidos
    const [llDisponibles, setLlDisponibles] = useState(false);//para listar disponibles
    const disponibles = () => setLlDisponibles(true);//para listar disponibles
    const [selectedId, setSelectedId] = useState(null);//PARA SACAR EL ID DE LA TABLA
    const [show2, setShow2] = useState(false);//Model Box2
    const handleeClose = () => setShow2(false);//Model Box2
    const handleeShow = () => setShow2(true);//Model Box2
    const handleEditarEstudiante = async (id) => {
        setSelectedId(id);//Guarda el id en la variable selectedId
        handleeShow();//Llama a el model de editar
    };//PARA SACAR EL ID DE LA TABLA

    const columns = [

        {
            name: 'NOMBRES',
            selector: row => row.nombres,
        },
        {
            name: 'APELLIDOS',
            selector: row => row.apellidos,
        },
        {
            name: 'IDENTIFICACION',
            selector: row => row.identificacion,
        },
        {
            name: 'TIPO IDENTIFICACION',
            selector: row => row.tipo_identificacion,
        },
        {
            name: 'DIRECCION',
            selector: row => row.direccion,
        },
        {
            name: 'TELEFONO',
            selector: row => row.telefono,
        },
       

        {
            name: 'Acciones',
            selector: row => (<>

                <div style={{ display: 'flex', gap: '10px' }}>
                    <a href="#" class="btn btn-outline-info btn-rounded" value={selectedId} onClick={() => handleEditarEstudiante(row.external_id)}>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-pencil-square" viewBox="0 0 16 16">
                            <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z" />
                            <path fillRule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z" />
                        </svg>
                    </a>
                </div>

            </>),
        },
    ];
    if (!llProfesores) {
        Profesores(getToken(), '/profesores').then((info) => {
            if (info.error === true || info.msg === 'Token no valido o expirado!') {
                borrarSession();
                mensajes(info.mensajes);
                navegation("/sesion");
            } else {
                setData(info.info);
            }
        })
        setLlProfesores(true);
    }

    return (

        <div className="container">
            <>
        <h1>PROFESORES</h1>
      </>
            <div className="crud shadow-lg p-3 mb-5 mt-5 bg-body rounded">
                <div className="row ">
                <div className="row ">
                    <div style={{ color: "#18122B" }}>
                        <h2><b>LISTADO DE </b></h2>
                    </div>
                </div>

                    <div className="col-sm-2 offset-sm-1  mt-5 mb-4 text-gred">
                        <Button variant="primary" onClick={handleShow} style={{ background: "#443C68" }}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus" viewBox="0 0 16 16">
                                <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
                            </svg>
                            <span style={{ marginLeft: '5px' }}>Agregar Estudiante</span>
                        </Button>
                    </div>
                </div>
                
                <div className="row">
                    <DataTable
                        columns={columns}
                        data={data}
                    />

                </div>
                <Footer></Footer>

                {/* <!--- Model Box ---> */}
                <div className="model_box">
                    <Modal
                        show={show}
                        onHide={handleClose}
                        backdrop="static"
                        keyboard={false}
                    >
                        <Modal.Header closeButton>
                            <Modal.Title>Agregar estudiante</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <RegistrarProfesor />
                        </Modal.Body>

                        <Modal.Footer>
                            <Button variant="secondary" onClick={handleClose}>
                                Cerrar
                            </Button>

                        </Modal.Footer>
                    </Modal>



                </div>

                {/* <!--- Model Box2 ---> */}
                <div className="model_box">
                    <Modal
                        show={show2}
                        onHide={handleeClose}
                        backdrop="static"
                        keyboard={false}
                    >
                        <Modal.Header closeButton>
                            <Modal.Title>Editar estudiante</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <EditarEstudiante nro={selectedId} ></EditarEstudiante>

                        </Modal.Body>

                        <Modal.Footer>
                            <Button variant="secondary" onClick={handleeClose}>
                                Cerrar
                            </Button>

                        </Modal.Footer>
                    </Modal>
                </div>
            </div>
        </div>
    );
}


export default ListarProfesor;