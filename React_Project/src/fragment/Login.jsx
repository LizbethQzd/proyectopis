import React from 'react';

import '../css/login.css';
import { useForm } from 'react-hook-form';
import { mensajes } from '../utilidades/Mensaje';
import { InicioSesion } from '../hooks/Conexion';
import { saveToken } from '../utilidades/Sessionutil';
import { useNavigate, useParams } from 'react-router-dom';
import unl from '../img/UNL.png';
import google from '../img/google.png';


const Login=()=> {
  const { register, handleSubmit } = useForm();
  const navegation = useNavigate();
  const onSubmit = (data) => {
    if (!data.email || !data.clave) {
      mensajes('Faltan datos', 'error', 'Campos vacíos');
      return;
    }

    var datos = {
      email: data.email,
      clave: data.clave
    };

    InicioSesion(datos)
      .then((info) => {
        if (info.msg === 'CLAVE INCORRECTA') {
          mensajes(info.message, 'error', 'Clave Incorrecta');
        } else if (info.msg === 'CUENTA NO ENCONTRADA') {
          mensajes(info.message, 'error', 'Cuenta no encontrada');
        } else {
          console.log(info);
          if (info.userType === "profesor") {

            mensajes(info.message, 'success', 'Exitoso');
            saveToken(info.token);
            navegation('/IndiceProfesor')
          } else if (info.userType === "estudiante") {
            mensajes(info.message, 'success', 'Exitoso');
            saveToken(info.token);
            navegation('/Indice');
          }
          else if (info.userType === "administrador") {
            mensajes(info.message, 'success', 'Exitoso');
            saveToken(info.token);
            navegation('/InicioAdmin');
          }
         
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
  
  return (
    <div>
      {/* particles.js container */}
      <div id="particles-js"></div>
      {/* Login */}
      <div className="loginBox">
      
        <div className="fromBox">
        <h1 className="title">Inicio de Sesión</h1>
          <div className="redes">
            
            <img src={unl} alt="iconounl" />
            <img src={google} alt="icongoogle" />
          </div>
          
          <form id="login" className="inputGroup">
            <div className="userBox">
            <input type="email" id="typeEmailX"{...register('email')} required />
        <label>Correo</label>
            </div>
            <div className="userBox">
            <input type="password"{...register('clave')} required />
                     <label>Contraseña</label>
            </div>
            <a onClick={handleSubmit(onSubmit)}>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              Acceder
            </a>
          </form>
        </div>
      </div>
    </div>
  );
}




export default Login;
