const Footer = () => {
    return (
    <div className="copyright text-center my-auto">
        <span>Copyright © Proyecto PIS</span>
    </div>
    );
}

export default Footer;