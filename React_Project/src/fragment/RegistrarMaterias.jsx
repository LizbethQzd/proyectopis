import React, { useState } from 'react';
import { useNavigate, Link } from 'react-router-dom';
//import { useNavigate } from 'react-router';
import { Profesor, GuardarMateria } from '../hooks/Conexion';
import { borrarSession, getToken } from '../utilidades/Sessionutil';
import mensajes from '../utilidades/Mensaje';
import { useForm } from 'react-hook-form';



export const RegistrarMaterias = () => {
  const { register, handleSubmit, formState: { errors } } = useForm();
  const navegation = useNavigate();
  const [profesor, setprofesor] = useState([]);//para listar profesor
  const [llprofesor, setLlprofesor] = useState(false);//para listar profesor
  const { watch, setValue } = useForm();//para listar profesor

  //acciones
  // onsubmit
  const onSubmit = (data) => {
    var datos = {
      'nombre': data.nombre,
      'external_profesor': data.external_profesor,
      'unidades': data.apellidos,
      

    };

    GuardarMateria(datos, getToken()).then((info) => {
      console.log(info);
      if (info.code !== 200) {
        mensajes(info.msg, 'error', 'Exitoso');
      } else {
        mensajes(info.msg);
      }
    });
    
  };
  

  if (!llprofesor) {
    Profesor(getToken()).then((info) => {
      if (info.error === true || info.msg === 'Token no valido o expirado!') {
        borrarSession();
        mensajes(info.msg);
        navegation("/sesion");
      } else {
        if (Array.isArray(info.info)) {
          setprofesor(info.info);
          setLlprofesor(true);
        } else if (typeof info.info === 'object') {
          setprofesor([info.info]);
          setLlprofesor(true);
        } else {
          console.error("No es un array válido");
        }
      }
    });
  }


  return (
    <div className="wrapper">
      <div className="d-flex flex-column">
        <div className="content">

          <div className='container-fluid'>
            <div className="col-lg-10">
              <div className="p-5">

                <form className="user" onSubmit={handleSubmit(onSubmit)}>
                  {/** INGRESAR EL NOMBRE */}
                  <div className="form-group">
                    <input type="text" className="form-control form-control-user" placeholder="Ingrese los nombre" {...register('nombre', { required: true })} />
                    {errors.nombre && errors.nombre.type === 'required' && <div className='alert alert-danger'>Ingrese el nombre</div>}
                  </div>
                 

                  {/** INGRESAR LA unidades */}
                  <div className="form-group">
                    <input type="number" className="form-control form-control-user" placeholder="Ingrese el num de unidades" {...register('unidades', { required: true })} />
                    {errors.unidades && errors.unidades.type === 'required' && <div className='alert alert-danger'>Ingrese la unidades</div>}
                  </div>

                  

                  

                  
                  
                  

                  {/** BOTÓN CANCELAR */}
                  <button type='submit' className="btn btn-success">GUARDAR</button>
                  <Link to='/auto' className="btn btn-danger" style={{ margin: 20 }}>CANCELAR</Link>

                </form>
                <hr />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default RegistrarMaterias;