import React, { useState } from 'react';
import { useNavigate, Link } from 'react-router-dom';
//import { useNavigate } from 'react-router';
import { Roles, GuardarEstudiante } from '../hooks/Conexion';
import { borrarSession, getToken } from '../utilidades/Sessionutil';
import mensajes from '../utilidades/Mensaje';
import { useForm } from 'react-hook-form';



export const RegistrarEstudiante = () => {
  const { register, handleSubmit, formState: { errors } } = useForm();
  const navegation = useNavigate();
  const [roles, setRoles] = useState([]);//para listar roles
  const [llroles, setLlroles] = useState(false);//para listar roles
  const { watch, setValue } = useForm();//para listar roles

  //acciones
  // onsubmit
  const onSubmit = (data) => {
    var datos = {
      'nombres': data.nombres,
      'external_rol': data.external_rol,
      'apellidos': data.apellidos,
      'identificacion': data.identificacion,
      'tipo_identificación': data.tipo_identificación,
      'direccion': data.direccion,
      'telefono': data.telefono,
      'genero': data.genero,
      'edad': data.edad,
      'email': data.email,
      'clave': data.clave

    };

    GuardarEstudiante(datos, getToken()).then((info) => {
      console.log(info);
      if (info.code !== 200) {
        mensajes(info.msg, 'error', 'Exitoso');
      } else {
        mensajes(info.msg);
      }
    });
    
  };
  

  if (!llroles) {
    Roles(getToken()).then((info) => {
      if (info.error === true || info.msg === 'Token no valido o expirado!') {
        borrarSession();
        mensajes(info.msg);
        navegation("/sesion");
      } else {
        if (Array.isArray(info.info)) {
          setRoles(info.info);
          setLlroles(true);
        } else if (typeof info.info === 'object') {
          setRoles([info.info]);
          setLlroles(true);
        } else {
          console.error("No es un array válido");
        }
      }
    });
  }


  return (
    <div className="wrapper">
      <div className="d-flex flex-column">
        <div className="content">

          <div className='container-fluid'>
            <div className="col-lg-10">
              <div className="p-5">

                <form className="user" onSubmit={handleSubmit(onSubmit)}>
                  {/** INGRESAR EL NOMBRE */}
                  <div className="form-group">
                    <input type="text" className="form-control form-control-user" placeholder="Ingrese los nombres" {...register('nombres', { required: true })} />
                    {errors.nombres && errors.nombres.type === 'required' && <div className='alert alert-danger'>Ingrese el nombre</div>}
                  </div>
                  {/** INGRESAR EL APELLIDO */}
                  <div className="form-group">
                    <input type="txt" className="form-control form-control-user" placeholder="Ingrese los apellidos" {...register('apellidos', { required: true })} />
                    {errors.apellidos && errors.apellidos.type === 'required' && <div className='alert alert-danger'>Ingrese los apellidos</div>}
                  </div>

                  {/** INGRESAR LA IDENTIFICACION */}
                  <div className="form-group">
                    <input type="number" className="form-control form-control-user" placeholder="Ingrese el num de identificacion" {...register('identificacion', { required: true })} />
                    {errors.identificacion && errors.identificacion.type === 'required' && <div className='alert alert-danger'>Ingrese la identificacion</div>}
                  </div>

                  {/** ESCOGER TIPO DE IDENTIFICACION*/}
                  <div className="form-group">
                    <input type="text" {...register('tipo_identificacion', { required: true })} className="form-control form-control-user" placeholder="Ingrese una identificacion" />
                    {errors.tipo_identificación && errors.tipo_identificación.type === 'required' && <div className='alert alert-danger'>Tipo de identificacion</div>}
                  </div>

                  {/** INGRESAR EL DIRECCION */}
                  <div className="form-group">
                    <input type="text" className="form-control form-control-user" placeholder="Ingrese la direccion" {...register('direccion', { required: true })} />
                    {errors.direccion && errors.direccion.type === 'required' && <div className='alert alert-danger'>Ingrese la direccion</div>}
                  </div>
                  {/** INGRESAR EL TELEFONO */}
                  <div className="form-group">
                    <input type="number" className="form-control form-control-user" placeholder="Ingrese el telefono" {...register('telefono', { required: true })} />
                    {errors.telefono && errors.telefono.type === 'required' && <div className='alert alert-danger'>Ingrese el telefono</div>}
                  </div>

                  {/** INGRESAR LA GENERO*/}
                  <div className="form-group">
                    <input type="text" className="form-control form-control-user" placeholder="Ingrese el genero" {...register('genero', { required: true })} />
                    {errors.genero && errors.genero.type === 'required' && <div className='alert alert-danger'>Ingrese el genero</div>}
                  </div>

                  {/** INGRESAR LA EDAD*/}
                  <div className="form-group">
                    <input type="number" className="form-control form-control-user" placeholder="Ingrese la edad" {...register('edad', { required: true })} />
                    {errors.edad && errors.edad.type === 'required' && <div className='alert alert-danger'>Ingrese la edad</div>}
                  </div>

                  {/** INGRESAR LA GENERO*/}
                  <div className="form-group">
                    <input type="text" className="form-control form-control-user" placeholder="Ingrese el correo" {...register('email', { required: true })} />
                    {errors.email && errors.email.type === 'required' && <div className='alert alert-danger'>Ingrese el correo</div>}
                  </div>
                  {/** INGRESAR LA GENERO*/}
                  <div className="form-group">
                    <input type="text" className="form-control form-control-user" placeholder="Ingrese la clave" {...register('clave', { required: true })} />
                    {errors.clave && errors.clave.type === 'required' && <div className='alert alert-danger'>Ingrese la clave</div>}
                  </div>
                  
                  

                  {/** BOTÓN CANCELAR */}
                  <button type='submit' className="btn btn-success">GUARDAR</button>
                  <Link to='/auto' className="btn btn-danger" style={{ margin: 20 }}>CANCELAR</Link>

                </form>
                <hr />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default RegistrarEstudiante;