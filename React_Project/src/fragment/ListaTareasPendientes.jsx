import React, { useEffect, useState } from 'react';
import { obtenerPractica } from '../hooks/Conexion';
import { borrarSession } from '../utilidades/Sessionutil';
import mensajes from '../utilidades/Mensaje';
import '../css/visualizar.css'
import 'bootstrap/dist/css/bootstrap.css';
import DataTable from 'react-data-table-component';

import { useNavigate } from 'react-router-dom';
function ListaTareasPendientes() {
  const ExpandedComponent = ({ data }) => <pre>{JSON.stringify(data, null, 2)}</pre>;
  const navigate = useNavigate();
  const [data, setDatos] = useState([]);
  const [estudiante, setEstudiante] = useState('');

  const handleListaTareas = () => {
    navigate("/AgregarPractica");
  }

  useEffect(() => {
    obtenerPractica()
      .then((info) => {
        if (info.error === true && info.mensaje === 'Acceso denegado. Token a expirado') {
          borrarSession();
          mensajes(info.mensaje);
          navigate("/Login");
        } else {

          console.log(info);
          setDatos(info);
          if (info.length > 0) {
            setEstudiante(info[0].estudiante.nombres);
          }
        }
      })
      .catch((error) => {
        console.error(error);
        // Manejar el error de alguna manera adecuada en tu aplicación
      });
  }, []);



  const columns = [
    {
      name: 'TEMA',
      selector: (row) => row.tema,
      sortable: true,
      style: {
        fontWeight: '#18122B', // Aplicar negrita al texto del título
      },
    },
    {
      name: 'DESCRIPCIÓN',
      selector: (row) => row.descripcion,
      sortable: true,
      style: {
        fontWeight: '#18122B', // Aplicar negrita al texto del título
      },
    },
    {
      name: 'FECHA DE ENTREGA',
      selector: (row) => row.fecha_entrega,
      sortable: true,
      style: {
        fontWeight: '#18122B', // Aplicar negrita al texto del título
      },
    },
    {
      name: 'Acciones',
      //onClick={() => handleModificar(row)}
      cell: (row) => (
        <button className="btn btn-primary" onClick={handleListaTareas}>
          ENTREGAR PRACTICA
        </button>
      ),
      sortable: true,
      style: {
        fontWeight: '#18122B', // Aplicar negrita al texto del título
      },
    },
    //  <ModificarAuto onModificarAuto={handleModificarAuto} />--->
  ];
  return (

    <div className="container">
      <>
        <h1>ESTUDIANTE</h1>
      </>
      <div className="crud shadow-lg p-3 mb-5 mt-5 bg-body rounded">
        <div className="row ">
          <div className="row ">
            <div style={{ color: "#18122B" }}>
              <h2><b>ACTIVIDADES A DESARROLLAR</b></h2>
            </div>
          </div>
          <div className="row">
            <DataTable
              columns={columns}
              data={data}
            />

          </div>
        </div>
      </div>
    </div>

  );
}

export default ListaTareasPendientes;
