import '../css/listaTareas.css'
import "bootstrap/dist/css/bootstrap.min.css";
import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { obtenerListaTareas } from '../hooks/Conexion';
import { ListaMaterias } from '../hooks/Conexion';
import { borrarSession } from '../utilidades/Sessionutil';
import mensajes from '../utilidades/Mensaje';
import { useNavigate, useParams } from 'react-router-dom';
import DataTable from 'react-data-table-component';

import EditarTarea from "./EditarTarea";
import { Button, Modal, Input } from 'react-bootstrap';

const TareasCreadas = () => {
  const ExpandedComponent = ({ data }) => <pre>{JSON.stringify(data, null, 2)}</pre>;
  const [data, setData] = useState([]);
  const navegation = useNavigate();
  const [docente, setDocente] = useState('');

  const { external } = useParams()
  const [selectedId, setSelectedId] = useState(null);//PARA SACAR EL ID DE LA TABLA
  const [show2, setShow2] = useState(false);//Model Box2
  const handleeClose = () => setShow2(false);//Model Box2
  const handleeShow = () => setShow2(true);//Model Box2
  const handleEditarTarea = async (id) => {
    setSelectedId(id);//Guarda el id en la variable selectedId
    handleeShow();//Llama a el model de editar
  };//PARA SACAR EL ID DE LA TABLA
  useEffect(() => {
    obtenerListaTareas()
      .then((info) => {
        if (info.error === true && info.mensaje === 'Acceso denegado. Token a expirado') {
          borrarSession();
          mensajes(info.mensaje);
          navegation("/Login");
        } else {
          console.log(info);
          setData(info);

        }
      })
      .catch((error) => {
        console.error(error);
        // Manejar el error de alguna manera adecuada en tu aplicación
      });
  }, []);
  const columns = [
    {
      name: 'Tema',
      selector: (row) => row.tema
    },
    {
      name: 'descripcion',
      selector: (row) => row.descripcion
    },
    {
      name: 'fecha_envio',
      selector: (row) => row.fecha_envio
    },
    {
      name: 'fecha_entrega',
      selector: (row) => row.fecha_entrega
    },

    {
      name: 'Acciones',
      selector: row => (<>

        <div style={{ display: 'flex', gap: '10px' }}>
          <a href="#" class="btn btn-outline-info btn-rounded" value={selectedId} onClick={() => handleEditarTarea(row.external_id)}>
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-pencil-square" viewBox="0 0 16 16">
              <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z" />
              <path fillRule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z" />
            </svg>
          </a>
        </div>

      </>),
    },
  ];
  const handleAgregarTarea = (external) => {
    navegation(`/CrearTareas/${external}`);
  };


  const handleModificarTarea = (external) => {
    navegation(`/ModificarTareas/${external}`);
  };
  const handleVerTarea = (external) => {
    navegation(`/VerTareas/${external}`);
  };
  return (
    <>
      <div className='col-12' id='usuario'><br />
        <center><h1>TAREAS</h1></center>
        <br />
        <center>
          <div className="input-group">
            <input type="text" className="form-control bg-light border-0 small" placeholder="Buscar" aria-label="Search" aria-describedby="basic-addon2" />
            <button className="btn btn-primary" type="button" style={{ margin: "0px", height: '40px' }}>Bucar</button>
          </div><br />

          <div className="row">
            <DataTable
              columns={columns}
              data={data}
            />

          </div>
          <div className='container-fluid'>
            <a className='btn btn-success' role="button" onClick={() => handleAgregarTarea(external)}>Registrar Tarea</a>
          </div>
        </center>
      </div>





    </>

  );
};

export default TareasCreadas;