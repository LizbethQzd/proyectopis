import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { obtenerTodo, ObtenerExternal } from '../hooks/Conexion';
import { ListaMaterias } from '../hooks/Conexion';
import { borrarSession } from '../utilidades/Sessionutil';
import mensajes from '../utilidades/Mensaje';
import { useNavigate } from 'react-router-dom';
import DataTable from 'react-data-table-component';
import Modal from 'react-bootstrap/Modal';

const InicioProfesor = () => {
  const ExpandedComponent = ({ data }) => <pre>{JSON.stringify(data, null, 2)}</pre>;
  const [data, setData] = useState([]);
  const navegation = useNavigate();
  const [docente, setDocente] = useState('');

  useEffect(() => {
    obtenerTodo()
      .then((info) => {
        if (info.error === true && info.mensaje === 'Acceso denegado. Token a expirado') {
          borrarSession();
          mensajes(info.mensaje);
          navegation("/sesion");
        } else {
          console.log(info);
          setData(info);
          if (info.length > 0) {
            setDocente(info[0].profesor.nombres);
          }
        }
      })
      .catch((error) => {
        console.error(error);
        // Manejar el error de alguna manera adecuada en tu aplicación
      });
  }, []);

  const columns = [
    {
      name: 'Materia',
      selector: (row) => row.materia.nombre
    },
    {
      name: 'Unidades',
      selector: (row) => row.materia.unidades
    },
    {
      name: 'Acciones',
      cell: (row) => (
        <button className="btn btn-primary" onClick={() => handleAgregarTarea(row.external_id)}>
          Asignar Practica
        </button>
      ),
    },
  ];

  const handleAgregarTarea = (external) => {
    navegation(`/TareasCreadas/${external}`);
  };

  return (

    <div className="container">
      <>
        <h1>DOCENTE:  {docente}</h1>
      </>
      <div className="crud shadow-lg p-3 mb-5 mt-5 bg-body rounded">
        <div className="row ">
          <div className="row ">
            <div style={{ color: "#18122B" }}>
              <h2><b>MATERIAS</b></h2>
            </div>
          </div>
          <div className="row">
                    <DataTable
                        columns={columns}
                        data={data}
                    />

                </div>
        </div>
      </div>
    </div>
  );
};

export default InicioProfesor;
