import React, { useState, useEffect } from 'react';
import {getToken } from '../utilidades/Sessionutil';
import mensajes from '../utilidades/Mensaje';
import { useForm } from 'react-hook-form';
import { useNavigate } from 'react-router';
import { ModificarEstudiante, ObtenerEstudiante,Roles} from '../hooks/Conexion';



function EditarEstudiante(nro) {
  const { register, handleSubmit, formState: { errors }, setValue } = useForm();
  const [validated, setValidated] = useState(false);
  const [a, setA] = useState(null);
  const navegation = useNavigate();

  const onSubmit = (data) => {
 
    var datos = {
      "external_id": a.external_id,
      "nombres": data.nombres,
      "apellidos": data.apellidos,
      "identificacion": data.identificacion,
      "tipo_identificación": data.tipo_identificación,
      "direccion": data.direccion,
      "telefono": data.telefono,
      "genero": data.genero,
      "edad": data.edad
    };
    //console.log(a.external_id);
    //console.log(datos);
  
    ModificarEstudiante(getToken(),datos).then((info) => {
      if (info.code !==200) {
        mensajes(info.msg, 'error', 'Error');
        console.log('Error: Edititando...')
      } else {
        mensajes(info.msg);
        navegation("/autoss");
        console.log('Exito: Edititando')
      }
    });
  };

  //se utiliza el hook useEffect para llamar a GetDatos(nro.nro) una vez cuando el componente se monta.
  useEffect(() => {
    ObtenerEstudiante(getToken(),nro.nro).then((resultado) => {
      setA(resultado.info);
      console.log(resultado.info);
    }).catch((error) => {
      console.error("Error: Obtener datos a editar", error);
    });
  }, []);

  return (
    <div className="wrapper">
    <div className="d-flex flex-column">
      <div className="content">

        <div className='container-fluid'>
          <div className="col-lg-10">
            <div className="p-5">

              <form className="user" onSubmit={handleSubmit(onSubmit)}>
                {/** INGRESAR EL NOMBRE */}
                <div className="form-group">
                  <input type="text" className="form-control form-control-user" placeholder="Ingrese los nombres" {...register('nombres', { required: true })} />
                  {errors.nombres && errors.nombres.type === 'required' && <div className='alert alert-danger'>Ingrese el nombre</div>}
                </div>
                {/** INGRESAR EL APELLIDO */}
                <div className="form-group">
                  <input type="txt" className="form-control form-control-user" placeholder="Ingrese los apellidos" {...register('apellidos', { required: true })} />
                  {errors.apellidos && errors.apellidos.type === 'required' && <div className='alert alert-danger'>Ingrese los apellidos</div>}
                </div>

                {/** INGRESAR LA IDENTIFICACION */}
                <div className="form-group">
                  <input type="number" className="form-control form-control-user" placeholder="Ingrese el num de identificacion" {...register('identificacion', { required: true })} />
                  {errors.identificacion && errors.identificacion.type === 'required' && <div className='alert alert-danger'>Ingrese la identificacion</div>}
                </div>

                {/** ESCOGER TIPO DE IDENTIFICACION*/}
                <div className="form-group">
                  <input type="text" {...register('tipo_identificacion', { required: true })} className="form-control form-control-user" placeholder="Ingrese una identificacion" />
                  {errors.tipo_identificación && errors.tipo_identificación.type === 'required' && <div className='alert alert-danger'>Tipo de identificacion</div>}
                </div>

                {/** INGRESAR EL DIRECCION */}
                <div className="form-group">
                  <input type="text" className="form-control form-control-user" placeholder="Ingrese la direccion" {...register('direccion', { required: true })} />
                  {errors.direccion && errors.direccion.type === 'required' && <div className='alert alert-danger'>Ingrese la direccion</div>}
                </div>
                {/** INGRESAR EL TELEFONO */}
                <div className="form-group">
                  <input type="number" className="form-control form-control-user" placeholder="Ingrese el telefono" {...register('telefono', { required: true })} />
                  {errors.telefono && errors.telefono.type === 'required' && <div className='alert alert-danger'>Ingrese el telefono</div>}
                </div>

                {/** INGRESAR LA GENERO*/}
                <div className="form-group">
                  <input type="text" className="form-control form-control-user" placeholder="Ingrese el genero" {...register('genero', { required: true })} />
                  {errors.genero && errors.genero.type === 'required' && <div className='alert alert-danger'>Ingrese el genero</div>}
                </div>

                {/** INGRESAR LA EDAD*/}
                <div className="form-group">
                  <input type="number" className="form-control form-control-user" placeholder="Ingrese la edad" {...register('edad', { required: true })} />
                  {errors.edad && errors.edad.type === 'required' && <div className='alert alert-danger'>Ingrese la edad</div>}
                </div>

                {/** INGRESAR LA GENERO*/}
                <div className="form-group">
                  <input type="text" className="form-control form-control-user" placeholder="Ingrese el correo" {...register('email', { required: true })} />
                  {errors.email && errors.email.type === 'required' && <div className='alert alert-danger'>Ingrese el correo</div>}
                </div>
                {/** INGRESAR LA GENERO*/}
                <div className="form-group">
                  <input type="text" className="form-control form-control-user" placeholder="Ingrese la clave" {...register('clave', { required: true })} />
                  {errors.clave && errors.clave.type === 'required' && <div className='alert alert-danger'>Ingrese la clave</div>}
                </div>
                
                

                {/** BOTÓN CANCELAR */}
                <button type='submit' className="btn btn-success">MODIFICAR</button>
                

              </form>
              <hr />
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  );
}
export default EditarEstudiante;