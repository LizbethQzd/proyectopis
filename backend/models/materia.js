'use strict';
const { DataTypes } = require('sequelize');

module.exports = (sequelize,DataTypes) => {
  const materia = sequelize.define('materia', {
    id_profesor: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    nombre: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    unidades: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 }
    
  }, {
    freezeTableName: true
  });

  materia.associate = function (models) {
    materia.belongsTo(models.profesor, {  foreignKey: 'id_profesor',  as: 'profesor'});
    materia.hasMany(models.cursa, {foreignKey: 'id_materia', as: 'cursa'});
  };

  return materia;
};
