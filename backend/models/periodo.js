'use strict';
const { DataTypes } = require('sequelize');

module.exports = (sequelize,DataTypes) => {
  const periodo = sequelize.define('periodo', {
    id_cursa: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 },
    
    mes_comienzo: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    mes_termina: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    fin_año: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    estado: { type: DataTypes.BOOLEAN, defaultValue: true },

  }, {
    freezeTableName: true
  });

  periodo.associate = function(models) {
    periodo.belongsTo(models.matricula, { foreignKey: 'id_matricula', as: 'matricula' });
  };

  return periodo;
};
