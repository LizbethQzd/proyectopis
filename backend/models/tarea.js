'use strict';
const { DataTypes } = require('sequelize');

module.exports = (sequelize,DataTypes) => {
  const tarea = sequelize.define('tarea', {
    id_cursa: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    tema: {
      type: DataTypes.STRING(50),
      allowNull: false,
    },
   
    descripcion: {
      type: DataTypes.STRING(300),
      allowNull: false,
    },
    fecha_envio: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    fecha_entrega: {
      type: DataTypes.DATE,
      allowNull: false,
    },

    
    external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 },
    estado: { type: DataTypes.BOOLEAN, defaultValue: true },
  }, {
    freezeTableName: true
  });

  tarea.associate = function(models) {
    tarea.belongsTo(models.cursa, { foreignKey: 'id_cursa', as: 'cursa' });
    tarea.hasMany(models.entregaTarea, { foreignKey: 'id_tarea', as: "entregaTarea" });
  };

  return tarea;
};
