'use strict';
const { DataTypes } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    const matricula = sequelize.define('matricula', {
        id_periodo: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        id_cursa: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        id_estudiante: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        fecha_matricula: {
            type: DataTypes.DATE,
            allowNull: false,
        },
        
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 },

        estado: { type: DataTypes.BOOLEAN, defaultValue: true },
    }, {
        freezeTableName: true
    });

    matricula.associate = function (models) {
        matricula.belongsTo(models.periodo, { foreignKey: 'id_periodo', as: 'periodo' });
        matricula.belongsTo(models.cursa, { foreignKey: 'id_cursa', as: 'cursa' });
        matricula.belongsTo(models.estudiante, { foreignKey: 'id_estudiante', as: 'estudiante' });
    };

    return matricula;
};