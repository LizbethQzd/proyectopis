'use strict';
const { DataTypes } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    const entregaTarea = sequelize.define('entregaTarea', {
        tema: {
            type: DataTypes.STRING(50),
            allowNull: false,
        },
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 },
        descripcion: {
            type: DataTypes.STRING(300),
            allowNull: false,
        },
        archivo: {
            type: DataTypes.JSON, allowNull: true,
        },
        estudiante: {
            type: DataTypes.STRING(12),
          },
        estado: { type: DataTypes.BOOLEAN, defaultValue: true },
    }, {
        freezeTableName: true
    });

    entregaTarea.associate = function (models) {
        entregaTarea.belongsTo(models.tarea, { foreignKey: 'id_tarea' });
    };

    return entregaTarea;
};
