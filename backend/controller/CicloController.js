'use strict';
var models = require('../models');
var cuenta = models.cuenta;
const { body, validationResult, check } = require('express-validator');
const bcypt = require('bcrypt');
const salRounds = 8;
let jwt = require('jsonwebtoken');

class CicloController {
    async listar(req, res) {
        try {
        var lista = await ciclo.findAll({
            attributes: ['external_id'],
            include: [
              {
                model: materia,
                as:'materia',
                attributes: ['nombre','unidades'],
              }
            ],
        });
        res.status(200);
        res.json({ msg: "0k", code: 200, info: lista });
    } catch (error) {
        console.log(error);
        res.status(500).json({ msg: "Ocurrió un error al obtener la información", code: 500 });
      }
    }




    async guardar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            var materia_id = req.body.external_materia;
         
            if (1===1) {
                let materiaAux = await materia.findOne({ where: { external_id: materia_id } });
                console.log(materiaAux);
                if (materiaAux) {
                    var data = {
                        paralelo: req.body.paralelo,
                        id_materia: materiaAux.id,
                    }
                    let transaction = await models.sequelize.transaction();
                    try {
                        await materia.create(data);
                        await transaction.commit();
                        res.json({ msg: "Se han registrado los datos", code: 200 });
                    } catch (error) {
                        if (transaction) await transaction.rollback();
                        if (error.errors && error.errors[0].message) {
                            res.json({ msg: error.errors[0].message, code: 200 });
                        } else {
                            res.json({ msg: error.message, code: 200 });
                        }
                    }
                } else {
                    res.status(400);
                    res.json({ msg: "Datos no encontrados", code: 400 });
                }
            } else {
                res.status(400);
                res.json({ msg: "Faltan datos", code: 400 });
            }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }




}
module.exports = CicloController;