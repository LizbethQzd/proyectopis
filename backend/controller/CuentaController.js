'use strict';

const models = require('../models');
const cuenta = models.cuenta;
const { validationResult } = require('express-validator');
const bcrypt = require('bcrypt');
const salRounds = 8;
const jwt = require('jsonwebtoken');

class CuentaController {
  async sesion(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ msg: 'Faltan datos', code: 400 });
    }

    try {
      const login = await cuenta.findOne({
        where: { email: req.body.email },
        include: [
          { model: models.estudiante, as: 'estudiante', attributes: ['nombres', 'apellidos'] },
          { model: models.profesor, as: 'profesor', attributes: ['nombres', 'apellidos'] },
          { model: models.administrador, as: 'administrador', attributes: ['nombres', 'apellidos'] }
        ]
      });

      if (!login) {
        return res.status(400).json({ msg: 'CUENTA NO ENCONTRADA', code: 400 });
      }

      const isClaveValida = (clave, claveUs) => {
        return bcrypt.compareSync(claveUs, clave);
      };

      if (login.estado) {
        let tipoUsuario;
        let user;
        
        if (login.estudiante) {
          tipoUsuario = 'estudiante';
          user = login.estudiante;
        } else if (login.profesor) {
          tipoUsuario = 'profesor';
          user = login.profesor;
        } else if (login.administrador) {
          tipoUsuario = 'administrador';
          user = login.administrador;
        }

        if (!tipoUsuario) {
          tipoUsuario = 'desconocido';
        }

        if (isClaveValida(login.clave, req.body.clave)) {
          const tokenData = {
            external: login.external_id,
            email: login.email,
            check: true,
            userType: tipoUsuario
          };

          const llave = process.env.KEY;
          const token = jwt.sign(tokenData, llave, { expiresIn: '1h' });

          return res.status(200).json({
            msg: 'OK!',
            code: 200,
            token: token,
            user: user ? user.nombres + ' ' + user.apellidos : 'Usuario no identificado',
            email: login.email,
            userType: tipoUsuario
          });
        } else {
          return res.status(400).json({ msg: 'CLAVE INCORRECTA', code: 400 });
        }
      } else {
        return res.status(400).json({ msg: 'CUENTA DESACTIVADA', code: 400 });
      }
    } catch (error) {
      console.error(error);
      return res.status(500).json({ msg: 'Error en el servidor', code: 500 });
    }
  }
}

module.exports = CuentaController;

