'use strict';
const { body, validationResult, check } = require('express-validator');
var models = require('../models');
var profesor = models.profesor;
var materia = models.materia;
const bcrypt = require('bcrypt');
const periodo = require('../models/periodo');
const salRounds = 8;
class PeriodoController {
   
    async guardar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            var cursa_id = req.body.external_cursa;
         
            if (1===1) {
                let cursaAux = await cursa.findOne({ where: { external_id: cursa_id } });
               
                if (cursaAux) {
                   

                    //data arreglo asociativo= es un direccionario = clave:valor
                    var data = {
                        mes_comienzo: req.body.mes_comienzo,
                        mes_termina: req.body.mes_termina,
                        fin_año: req.body.fin_año,
                        id_cursa: cursaAux.id,
                    }
                    let transaction = await models.sequelize.transaction();
                    try {
                        await periodo.create(data);
                        await transaction.commit();
                        res.json({ msg: "Se han registrado los datos", code: 200 });
                    } catch (error) {
                        if (transaction) await transaction.rollback();
                        if (error.errors && error.errors[0].message) {
                            res.json({ msg: error.errors[0].message, code: 200 });
                        } else {
                            res.json({ msg: error.message, code: 200 });
                        }
                    }
                } else {
                    res.status(400);
                    res.json({ msg: "Datos no encontrados", code: 400 });
                }
            } else {
                res.status(400);
                res.json({ msg: "Faltan datos", code: 400 });
            }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }
}
module.exports = PeriodoController;