'use strict';
const { body, validationResult, check } = require('express-validator');
var models = require('../models/');
var administrador = models.administrador;
var rol = models.rol;
const bcrypt = require('bcrypt');
const salRounds = 8;

class AdministradorController {
   


    async guardar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            var rol_id = req.body.external_rol;
         
            if (1===1) {
                let rolAux = await rol.findOne({ where: { external_id: rol_id } });
                console.log(rolAux);
                console.log(req.body.email);
                if (rolAux) {
                    var claveHash = function (clave) {
                        return bcrypt.hashSync(clave, bcrypt.genSaltSync(salRounds), null);
                    };
                    //data arreglo asociativo= es un direccionario = clave:valor
                    var data = {
                        identificacion: req.body.identificacion,
                        tipo_identificacion: req.body.tipo_identificacion,
                        nombres: req.body.nombres,
                        edad: req.body.edad,
                        apellidos: req.body.apellidos,
                        direccion: req.body.direccion,
                        genero: req.body.genero,
                        telefono:req.body.telefono,
                        id_rol: rolAux.id,
                        cuenta: {
                            email: req.body.email,
                            clave: claveHash(req.body.clave)
                        }
                    }
                    let transaction = await models.sequelize.transaction();
                    try {
                        await administrador.create(data, { include: [{ model: models.cuenta, as: "cuenta" }], transaction });
                        await transaction.commit();
                        res.json({ msg: "Se han registrado los datos", code: 200 });
                    } catch (error) {
                        if (transaction) await transaction.rollback();
                        if (error.errors && error.errors[0].message) {
                            res.json({ msg: error.errors[0].message, code: 200 });
                        } else {
                            res.json({ msg: error.message, code: 200 });
                        }
                    }
                } else {
                    res.status(400);
                    res.json({ msg: "Datos no encontrados", code: 400 });
                }
            } else {
                res.status(400);
                res.json({ msg: "Faltan datos", code: 400 });
            }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }
    
    async modificar(req, res) {
        var person = await administrador.finOne({ where: { external_id: req.body.external } });
        if (administrador === null) {
            res.status(400);
            res.json({ msg: "No existe el registro", code: 400 });
        } else {
            var uuid = require('uuid');
            person.identificacion = req.body.identificacion;
            person.tipo_identificacion = req.dody.tipo_identificacion;
            person.nombres = req.body.nombres;
            person.edad = req.body.edad;
            person.apellidos = req.body.apellidos;
            person.direccion = req.body.direccion;
            person.genero = req.body.genero;
            person.telefono = req.body.telefono;
            person.id_rol = req.body.id;
            person.external_id = uuid.v4();
            var result = await person.save();
            if (result === null) {
                res.status(400);
                res.json({ msg: 'No se ha modificado sus datos', code: 400 });

            } else {
                res.status(200);
                res.json({ msg: "se han modificado sus datos", code: 200 });
            }
        }
    }

    
}
module.exports = AdministradorController;