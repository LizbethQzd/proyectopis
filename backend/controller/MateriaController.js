'use strict';
const { body, validationResult, check } = require('express-validator');
var models = require('../models');
var profesor = models.profesor;
var materia = models.materia;
const bcrypt = require('bcrypt');
const salRounds = 8;
class MateriaController {
    async listar(req, res) {
        try {
        var lista = await materia.findAll({
            attributes: ['external_id'],
            include: [
              {
                model: profesor,
                as:'materia',
                attributes: ['nombres','apellidos'],
              }
            ],
        });
        res.status(200);
        res.json({ msg: "0k", code: 200, info: lista });
    } catch (error) {
        console.log(error);
        res.status(500).json({ msg: "Ocurrió un error al obtener la información", code: 500 });
      }
    }

    async guardar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            var profesor_id = req.body.external_profesor;
         
            if (1===1) {
                let profesorAux = await profesor.findOne({ where: { external_id: profesor_id } });
                console.log(profesorAux);
                console.log(req.body.email);
                if (profesorAux) {
                    
                    var data = {
                        nombre: req.body.nombre,
                        unidades: req.body.unidades,
                        id_profesor: profesorAux.id,
                    }
                    let transaction = await models.sequelize.transaction();
                    try {
                        await materia.create(data);
                        await transaction.commit();
                        res.json({ msg: "Se han registrado los datos", code: 200 });
                    } catch (error) {
                        if (transaction) await transaction.rollback();
                        if (error.errors && error.errors[0].message) {
                            res.json({ msg: error.errors[0].message, code: 200 });
                        } else {
                            res.json({ msg: error.message, code: 200 });
                        }
                    }
                } else {
                    res.status(400);
                    res.json({ msg: "Datos no encontrados", code: 400 });
                }
            } else {
                res.status(400);
                res.json({ msg: "Faltan datos", code: 400 });
            }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }

    async modificar(req, res) {
        var person = await estudiante.finOne({ where: { external_id: req.body.external } });
        if (estudiante === null) {
            res.status(400);
            res.json({ msg: "No existe el registro", code: 400 });
        } else {
            var uuid = require('uuid');
            person.identificacion = req.body.identificacion;
            person.tipo_identificacion = req.dody.tipo_identificacion;
            person.nombres = req.body.nombres;
            person.edad = req.body.edad;
            person.apellidos = req.body.apellidos;
            person.direccion = req.body.direccion;
            person.genero = req.body.genero;
            person.telefono = req.body.telefono;
            person.id_rol = req.body.id;
            person.external_id = uuid.v4();
            var result = await person.save();
            if (result === null) {
                res.status(400);
                res.json({ msg: 'No se ha modificado sus datos', code: 400 });

            } else {
                res.status(200);
                res.json({ msg: "se han modificado sus datos", code: 200 });
            }
        }
    }
}
module.exports = MateriaController;