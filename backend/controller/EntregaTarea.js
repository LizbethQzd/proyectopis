'use strict';
const axios = require('axios');
var models = require('../models');
var entregaTarea = models.entregaTarea;
const path = require('path');
const fs = require('fs/promises');
const { Console } = require('console');
class EntregableController {
    async listar(req, res) {
        var listar = await entregaTarea.findAll({
            include: { model: models.practica, foreignKey: 'id_practica', attributes: ['titulo', 'external_id', 'descripcion'] },
            attributes: ['nombre', 'external_id', 'descripcion', 'trabajo', 'estado', 'estudiante', 'createdAt']
        });
        res.json({ msg: 'OK!', code: 200, info: listar });
    }
    
   
    
    async guardar(req, res) {
        const documento = req.file;
        console.log(req.body.identificacion + '---------a');
        let rperAux = await models.estudiante.findOne({ where: { identificacion: req.body.identificacion } });

        let pract = await models.practica.findOne({ where: { external_id: req.body.external_id } });
        //     console.log('----as----------');
        let transaction = await models.sequelize.transaction();
        if (rperAux && (documento || (req.body.archivo !== null))) {
            try {
                const data = {
                    "archivo": req.body.archivo,
                    "estudiante": req.body.identificacion,
                    "tema": req.body.tema,
                    "descripcion": req.body.descripcion,
                    "id_practica": pract.id
                }
                
                //    console.log(data + '-----------------------------------');
                await models.entregaTarea.create(data, transaction);
                await transaction.commit();
                res.json({
                    msg: "PRACTICA ENTREGADA",
                    code: 200
                });
            } catch (error) {
                if (transaction) await transaction.rollback();
                if (error.errors && error.errors[0].message) {
                    res.json({ msg: error.errors[0].message, code: 400 });
                } else {
                    res.json({ msg: error.message, code: 400 });
                }
            }
        } else {
            res.json({ msg: 'estudiante no econtrada', code: 400 });
        }
    }
   

    

    
}

module.exports = EntregableController;