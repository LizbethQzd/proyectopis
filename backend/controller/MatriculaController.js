'use strict';
var models = require('../models');
var cuenta = models.cuenta;
const { body, validationResult, check } = require('express-validator');
const bcypt = require('bcrypt');
const salRounds = 8;
let jwt = require('jsonwebtoken');


class MatriculaController {

    async extraer(req, res) {
        try {
        var lista = await matricula.findAll({
            attributes: ['external_id'],
            include: [
              {
                model: periodo,
                as:'periodo',
                attributes: ['mes_comienzo','mes_termina'],
              },
              {
                model: estudiante,
                as:'estudiante',
                attributes: ['nombres'],
              },
              {
                model: cursa,
                as:'cursa',
                attributes: ['nota'],
              }
            ],
        });
        res.status(200);
        res.json({ msg: "0k", code: 200, info: lista });
    } catch (error) {
        console.log(error);
        res.status(500).json({ msg: "Ocurrió un error al obtener la información", code: 500 });
      }
    }

    async guardar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {

            var estudiante_id = req.body.external_estudiante;
            var periodo_id = req.body.external_periodo;
            var cursa_id = req.body.external_cursa;
            let estuAux = await estudiante.findOne({ where: { external_id: estudiante_id } });
            let periodoAux = await periodo.findOne({ where: { external_id: periodo_id } });
            let cursaAux = await cursa.findOne({ where: { external_id: cursa_id } });
            console.log(matAux);
            //data arreglo asociativo= es un direccionario = clave:valor
            var data = {
                id_periodo: periodoAux.id,
                id_cursa: cursaAux.id,
                id_estudiante: estuAux.id,
                fecha_matricula: req.body.fechaMatricula,
            }
            let transaction = await models.sequelize.transaction();
            try {
                await matricula.create(data);
                await transaction.commit();
                res.json({ msg: "Se han registrado los datos", code: 200 });
            } catch (error) {
                if (transaction) await transaction.rollback();
                if (error.errors && error.errors[0].message) {
                    res.json({ msg: error.errors[0].message, code: 200 });
                } else {
                    res.json({ msg: error.message, code: 200 });
                }
            }

        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }




}
module.exports = MatriculaController;