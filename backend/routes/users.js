var express = require('express');
var router = express.Router();
const { body, validationResult } = require('express-validator');
const RolController = require('../controller/RolController');
const EstudianteController = require('../controller/EstudianteController');
const ProfesorController = require('../controller/ProfesorController');
var rolController = new RolController();
var estudianteController = new EstudianteController();
var profesorController = new ProfesorController();
const CuentaController = require('../controller/CuentaController');
var cuentaController = new CuentaController();
const MateriaController = require('../controller/MateriaController');
var materiaController = new MateriaController();
const TareaController = require('../controller/TareaController');
var tareaController = new TareaController();
const CursaController = require('../controller/CursaController');
var cursaController = new CursaController();
const AdministradorController = require('../controller/AdministradorController');
var administradorController = new AdministradorController();

//Middleware||Falta autorizacion para roles
var auth=function middleware(req,res,next){
  const token=req.headers['x-api-token'];
  console.log(req.headers);
  if (token) {
    require('dotenv').config();
    const llave = process.env.KEY;
    jwt.verify(token,llave,async (err,decoded)=>{
      if (err) {
        res.status(401);
        res.json({ msg: "Token no valido o expirado!", code: 401 });
      }else{
        var models=require('../models');
        var cuenta=models.cuenta;
        req.decoded=decoded; 
        let aux= await cuenta.findOne({where:{external_id:req.decoded.external}});
        if (aux===null) {
          res.status(401);
          res.json({ msg: "Token no valido!", code: 401 });
        } else {
          next();
        }
      }
    })
  } else {
    res.status(401);
    res.json({ msg: "No existe token", code: 401 });
  }
}

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.json({ "version": "1.0", "name": "auto" });
});
//ROLES
router.get('/roles', rolController.listar);
//ESTUDIANTE
router.post('/estudiantes/modificar', estudianteController.modificar);

router.post('/estudiantes/guardar', [
  body('apellidos', 'Ingrese su apellido').exists(),
  body('nombres', 'Ingrese algun dato').exists()
], estudianteController.guardar);

router.get('/estudiantes', estudianteController.listar);
router.get('/estudiantes/obtener/:external', estudianteController.obtener);
//CURSA
router.post('/cursa/guardar', cursaController.guardar);
router.get('/cursa/external', cursaController.extraerIdCursa);
router.get('/cursa/tareas', cursaController.extraerNombreTarea);
router.get('/cursa/listar', cursaController.extraerTodo);
router.get('/', function (req, res, next) {
  res.json({ "version": "1.0", "name": "auto" });
});
//PROFESOR
router.post('/profesor/guardar', [
  body('apellidos', 'Ingrese su apellido').exists(),
  body('nombres', 'Ingrese algun dato').exists()
], profesorController.guardar);
router.post('/administrador/guardar', [
  body('apellidos', 'Ingrese su apellido').exists(),
  body('nombres', 'Ingrese algun dato').exists()
], administradorController.guardar);

router.post('/profesor/modificar', profesorController.modificar);
router.get('/profesor', profesorController.listar);
router.get('/profesor/obtener', profesorController.obtener);
//TAREA
router.post('/tarea/guardar', tareaController.guardar);
router.post('/tarea/modficar', tareaController.modificar);
router.get('/tarea/obtener/:external_id',tareaController.obtener);
router.get('/practicas/listar',tareaController.listar);
router.get('/tarea/obtener/:external',tareaController.obtener);
//SESION
router.post('/sesion', cuentaController.sesion);
//MATERIA
router.post('/materia/guardar', materiaController.guardar);

//rol
router.get('/roles',auth,rolController.listar);


module.exports = router;